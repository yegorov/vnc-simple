﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace MyServerVNC
{
    class Program
    {
        static Socket accept = null;
        static Bitmap ScreenImg = new Bitmap(10,10);
        static ImageConverter converter = new ImageConverter();

        static void Main(string[] args)
        {

            while (true)
            {
                Socket listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                //IPHostEntry ipHostInfo = Dns.GetHostEntry("127.0.0.1"); //Dns.Resolve(Dns.GetHostName());
                //IPAddress ipAddress = ipHostInfo.AddressList[0];
                //IPEndPoint localEndPoint = new IPEndPoint(ipAddress, 51000);

                IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, 51000);

                try
                {

                    listener.Bind(localEndPoint);
                    listener.Listen(1);
                    Console.WriteLine("Listen");

                    accept = listener.Accept();

                    while (true)
                    {
                        Console.WriteLine("GetCommand");
                        GetCommand();

                        if (!accept.Connected) break;

                    }

                    listener.Close();
                    accept.Close();
                }
                catch (Exception e)
                {
                    listener.Close();
                    accept.Close();
                    Console.WriteLine(e.ToString());
                    
                }
            }
        }

        private static void SendScreen()
        {
            ScreenImg.Dispose();
            ScreenImg = ImageFromScreen();

            accept.Send(ImageToByte(ScreenImg));           
        }

        private static void GetCommand()
        {
            Console.WriteLine("Receive");
            Byte[] rec = new Byte[12];
            accept.Receive(rec);
            int command = BitConverter.ToInt32(rec, 0);
            int x = BitConverter.ToInt32(rec, 4);
            int y = BitConverter.ToInt32(rec, 8);

            ExecCommand(command, x, y);
        }
        private static void ExecCommand(int c, int x, int y) 
        {
            Console.WriteLine("ExecCommand: " + c + " x=" + x + ", y=" + y);
            switch (c)
            {
                case 0:
                    Console.WriteLine("Error");
                    Console.ReadLine();
                    break;
                case 1:
                    SendScreen();
                    break;
                case 11:
                    LeftMouseClick(x, y);
                    break;
                case 12:
                    DoubleLeftMouseClick(x, y);
                    break;
                case 13:
                    RightMouseClick(x, y);
                    break;
                default:
                    break;
            }
        }

        //This is a replacement for Cursor.Position in WinForms
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        static extern bool SetCursorPos(int x, int y);

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern void mouse_event(int dwFlags, int dx, int dy, int cButtons, int dwExtraInfo);

        public const int MOUSEEVENTF_LEFTDOWN = 0x02;
        public const int MOUSEEVENTF_LEFTUP = 0x04;
        public const int MOUSEEVENTF_RIGHTDOWN = 0x08;
        public const int MOUSEEVENTF_RIGHTUP = 0x10;

        //This simulates a left mouse click
        public static void LeftMouseClick(int xpos, int ypos)
        {
            SetCursorPos(xpos, ypos);
            mouse_event(MOUSEEVENTF_LEFTDOWN, xpos, ypos, 0, 0);
            System.Threading.Thread.Sleep(10);
            mouse_event(MOUSEEVENTF_LEFTUP, xpos, ypos, 0, 0);
        }

        private static void DoubleLeftMouseClick(int xpos, int ypos)
        {
            SetCursorPos(xpos, ypos);
            mouse_event(MOUSEEVENTF_LEFTDOWN, xpos, ypos, 0, 0);
            mouse_event(MOUSEEVENTF_LEFTUP, xpos, ypos, 0, 0);
            System.Threading.Thread.Sleep(10);
            mouse_event(MOUSEEVENTF_LEFTDOWN, xpos, ypos, 0, 0);
            mouse_event(MOUSEEVENTF_LEFTUP, xpos, ypos, 0, 0);
        }

        public static void RightMouseClick(int xpos, int ypos)
        {
            SetCursorPos(xpos, ypos);
            mouse_event(MOUSEEVENTF_RIGHTDOWN, xpos, ypos, 0, 0);
            System.Threading.Thread.Sleep(10);
            mouse_event(MOUSEEVENTF_RIGHTUP, xpos, ypos, 0, 0);
        }

        public static byte[] ImageToByte(Image img)
        {
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }


        public static Bitmap ImageFromScreen()
        {
            Bitmap bmp = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height,
                PixelFormat.Format32bppRgb);
            using (Graphics gr = Graphics.FromImage(bmp))
            {
                gr.CopyFromScreen(Screen.PrimaryScreen.Bounds.X, Screen.PrimaryScreen.Bounds.Y,
                    0, 0, Screen.PrimaryScreen.Bounds.Size, CopyPixelOperation.SourceCopy);
            }
            return bmp;
        }
    }
}
