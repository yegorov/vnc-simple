﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyClientVNC
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private Point p;
        private Point unscaled_p;

        // image and container dimensions
        private int w_i;
        private int h_i;
        private int w_c;
        private int h_c;

        private Byte[] buf1;
        private Byte[] buf2;
        private Byte[] buf3;
        private Byte[] imgBuffer = new Byte[10000000];

        private Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        private void Form1_Load(object sender, EventArgs e)
        {
            pictureBox1.Image = new Bitmap("vnc.png");

            //IPHostEntry ipHostInfo = Dns.GetHostEntry("127.0.0.1"); 
            //IPAddress ipAddress = ipHostInfo.AddressList[0];
            //IPEndPoint localEndPoint = new IPEndPoint(new IPEndPoint(ipAddress, 51001);

            /*
            IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, 51001);
            socket.Bind(localEndPoint);

            socket.Connect("127.0.0.1", 51000);

            //socket.Connect("192.168.1.100", 51000);
            //socket.Connect("192.168.1.102", 51000);

            socket.ReceiveTimeout = 7000;

            pictureBox1.Image = new Bitmap(10, 10);

            timer1.Interval = 2000;
            timer1.Tick += timer1_Tick;
            timer1.Start();

            UpdateScreen();
            GetScreen();
            */
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            UpdateScreen();
            GetScreen();
        }

        private void GetScreen()
        {
            try
            {
                socket.Receive(imgBuffer);
                pictureBox1.Image.Dispose();
                pictureBox1.Image = Image.FromStream(new MemoryStream(imgBuffer));
            }
            catch(Exception) 
            {

            }

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Point pt = ImageCord();
            //Cursor.Position = pt; баг перетаскивал курсор на локальной машине
            MouseEventArgs me = (MouseEventArgs)e;
            if (me.Button == MouseButtons.Left)
                LeftMouseClick(pt.X, pt.Y);
            else if (me.Button == MouseButtons.Right)
                RightMouseClick(pt.X, pt.Y);
        }

        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {
            Point pt = ImageCord();
            //Cursor.Position = pt; баг перетаскивал курсор на локальной машине
            DoubleLeftMouseClick(pt.X, pt.Y);
        }

        private void RightMouseClick(int p1, int p2)
        {
            buf1 = BitConverter.GetBytes((int)13);
            buf2 = BitConverter.GetBytes(p1);
            buf3 = BitConverter.GetBytes(p2);
            socket.Send(buf1.Concat(buf2).Concat(buf3).ToArray());
        }

        private void LeftMouseClick(int p1, int p2)
        {
            buf1 = BitConverter.GetBytes((int)11);
            buf2 = BitConverter.GetBytes(p1);
            buf3 = BitConverter.GetBytes(p2);
            socket.Send(buf1.Concat(buf2).Concat(buf3).ToArray());
        }

        private void DoubleLeftMouseClick(int p1, int p2)
        {
            buf1 = BitConverter.GetBytes((int)12);
            buf2 = BitConverter.GetBytes(p1);
            buf3 = BitConverter.GetBytes(p2);
            socket.Send(buf1.Concat(buf2).Concat(buf3).ToArray());
        }

        private void UpdateScreen()
        {
            buf1 = BitConverter.GetBytes((int)1);
            buf2 = BitConverter.GetBytes(0);
            buf3 = BitConverter.GetBytes(0);
            socket.Send(buf1.Concat(buf2).Concat(buf3).ToArray());
            //SocketAsyncEventArgs e = new SocketAsyncEventArgs();
            //e.SetBuffer(buf1.Concat(buf2).Concat(buf3).ToArray(), 0, 12);
            //socket.SendAsync(e);
        }

        private Point ImageCord()
        {
            p = pictureBox1.PointToClient(Cursor.Position);
            unscaled_p = new Point();

            // image and container dimensions
            w_i = pictureBox1.Image.Width;
            h_i = pictureBox1.Image.Height;
            w_c = pictureBox1.Width;
            h_c = pictureBox1.Height;


            float imageRatio = w_i / (float)h_i; // image W:H ratio
            float containerRatio = w_c / (float)h_c; // container W:H ratio

            if (imageRatio >= containerRatio)
            {
                // horizontal image
                float scaleFactor = w_c / (float)w_i;
                float scaledHeight = h_i * scaleFactor;
                // calculate gap between top of container and top of image
                float filler = Math.Abs(h_c - scaledHeight) / 2;
                unscaled_p.X = (int)(p.X / scaleFactor);
                unscaled_p.Y = (int)((p.Y - filler) / scaleFactor);
            }
            else
            {
                // vertical image
                float scaleFactor = h_c / (float)h_i;
                float scaledWidth = w_i * scaleFactor;
                float filler = Math.Abs(w_c - scaledWidth) / 2;
                unscaled_p.X = (int)((p.X - filler) / scaleFactor);
                unscaled_p.Y = (int)(p.Y / scaleFactor);
            }

            return unscaled_p;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, 51001);
            socket.Bind(localEndPoint);
            socket.ReceiveTimeout = 7000;
            int port;
            socket.Connect(textBox1.Text, Int32.TryParse(textBox1.Text, out port) ? port : 51000);

            UpdateScreen();
            GetScreen();

            timer1.Interval = 4000;
            timer1.Tick += timer1_Tick;
            timer1.Start();
        }


    }
}
