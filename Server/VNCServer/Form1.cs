﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VNCServer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();          
        }

        private Point p;
        private Point unscaled_p;

        // image and container dimensions
        private int w_i;
        private int h_i;
        private int w_c;
        private int h_c;

        private void Form1_Load(object sender, EventArgs e)
        {
            pictureBox1.Image = ImageFromScreen();

            timer1.Interval = 5;
            timer1.Tick += timer1_Tick;
            timer1.Start();
        }


        public static byte[] ImageToByte(Image img)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            pictureBox1.Image.Dispose(); // Если не написать, то пол гига оперативы занимает за 5 сек.
            pictureBox1.Image = ImageFromScreen();
        }

        public Bitmap ImageFromScreen()
        {
            Bitmap bmp = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height,
                PixelFormat.Format32bppRgb);
            using (Graphics gr = Graphics.FromImage(bmp))
            {
                gr.CopyFromScreen(Screen.PrimaryScreen.Bounds.X, Screen.PrimaryScreen.Bounds.Y,
                    0, 0, Screen.PrimaryScreen.Bounds.Size, CopyPixelOperation.SourceCopy);
            }
            return bmp;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(ImageCord().ToString());
            Point pt = ImageCord();
            Cursor.Position = pt;
            MouseEventArgs me = (MouseEventArgs)e;
            if(me.Button == MouseButtons.Left)
                LeftMouseClick(pt.X, pt.Y);
            else if(me.Button == MouseButtons.Right)
                RightMouseClick(pt.X, pt.Y);
        }

        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {
            //MessageBox.Show(ImageCord().ToString());
            Point pt = ImageCord();
            Cursor.Position = pt;
            DoubleLeftMouseClick(pt.X, pt.Y);

        }

        private Point ImageCord()
        {
            p = pictureBox1.PointToClient(Cursor.Position);
            unscaled_p = new Point();

            // image and container dimensions
            w_i = pictureBox1.Image.Width;
            h_i = pictureBox1.Image.Height;
            w_c = pictureBox1.Width;
            h_c = pictureBox1.Height;


            float imageRatio = w_i / (float)h_i; // image W:H ratio
            float containerRatio = w_c / (float)h_c; // container W:H ratio

            if (imageRatio >= containerRatio)
            {
                // horizontal image
                float scaleFactor = w_c / (float)w_i;
                float scaledHeight = h_i * scaleFactor;
                // calculate gap between top of container and top of image
                float filler = Math.Abs(h_c - scaledHeight) / 2;
                unscaled_p.X = (int)(p.X / scaleFactor);
                unscaled_p.Y = (int)((p.Y - filler) / scaleFactor);
            }
            else
            {
                // vertical image
                float scaleFactor = h_c / (float)h_i;
                float scaledWidth = w_i * scaleFactor;
                float filler = Math.Abs(w_c - scaledWidth) / 2;
                unscaled_p.X = (int)((p.X - filler) / scaleFactor);
                unscaled_p.Y = (int)(p.Y / scaleFactor);
            }

            return unscaled_p;
        }

        //This is a replacement for Cursor.Position in WinForms
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        static extern bool SetCursorPos(int x, int y);

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern void mouse_event(int dwFlags, int dx, int dy, int cButtons, int dwExtraInfo);

        public const int MOUSEEVENTF_LEFTDOWN = 0x02;
        public const int MOUSEEVENTF_LEFTUP = 0x04;
        public const int MOUSEEVENTF_RIGHTDOWN = 0x08;
        public const int MOUSEEVENTF_RIGHTUP = 0x10;

        //This simulates a left mouse click
        public static void LeftMouseClick(int xpos, int ypos)
        {
            System.Threading.Thread.Sleep(500);

            SetCursorPos(xpos, ypos);
            mouse_event(MOUSEEVENTF_LEFTDOWN, xpos, ypos, 0, 0);
            System.Threading.Thread.Sleep(300);
            mouse_event(MOUSEEVENTF_LEFTUP, xpos, ypos, 0, 0);
        }

        private void DoubleLeftMouseClick(int xpos, int ypos)
        {
            System.Threading.Thread.Sleep(500);

            SetCursorPos(xpos, ypos);
            mouse_event(MOUSEEVENTF_LEFTDOWN, xpos, ypos, 0, 0);
            mouse_event(MOUSEEVENTF_LEFTUP, xpos, ypos, 0, 0);
            System.Threading.Thread.Sleep(300);
            mouse_event(MOUSEEVENTF_LEFTDOWN, xpos, ypos, 0, 0);
            mouse_event(MOUSEEVENTF_LEFTUP, xpos, ypos, 0, 0);
        }

        public static void RightMouseClick(int xpos, int ypos)
        {
            System.Threading.Thread.Sleep(500);

            SetCursorPos(xpos, ypos);
            mouse_event(MOUSEEVENTF_RIGHTDOWN, xpos, ypos, 0, 0);
            System.Threading.Thread.Sleep(300);
            mouse_event(MOUSEEVENTF_RIGHTUP, xpos, ypos, 0, 0);
        }
        
    }

}
/**
 * autosize tableLayoutPanel Anchor and Dock
 * http://www.cyberforum.ru/csharp-beginners/thread235768.html
 * http://stackoverflow.com/questions/10473582/how-to-retrieve-zoom-factor-of-a-winforms-picturebox
 * http://stackoverflow.com/questions/8272681/how-can-i-simulate-a-mouse-click-at-a-certain-position-on-the-screen
 * http://stackoverflow.com/questions/7350679/convert-a-bitmap-into-a-byte-array
 * 
 * http://www.codeproject.com/Articles/15460/C-Image-to-Byte-Array-and-Byte-Array-to-Image-Conv
 * 
 * https://msdn.microsoft.com/en-us/library/fx6588te%28v=vs.110%29.aspx
*/